= Changelog
:issue:  https://gitlab.com/ysb33rOrg/terraform-gradle-plugin/issues/
:mr:  https://gitlab.com/ysb33rOrg/terraform-gradle-plugin/merge_requests/

== v0.10.0

// tag::changelog[]
=== Features

* {issue}16[#16] - Improved up to date checking for various Terraform tasks.
* {issue}25[#25] - File paths printed in console are shown as URIs to support better hyperlinking from IDEs.
* {issue}28[#28] - Remote S3 state can be configured on a per source set basis.
* {issue}29[#29] - Tasks `tfUpgrade012` and `tfUpgrade013` added to aid in upgrading from previous `terraform` sources.
* {issue}32[#32] - Remove logging levels as it is not supported by Terraform and replace by method `setLogProgress`.

=== Other

* Default `terraform` version is `0.14.3`.
// end::changelog[]

== v0.9.0

=== Features

* {issue}22[#22] - `tfInit` can now take `--force-copy` and `--reconfigure` ans command-line options.
* {issue}23[#23] - Support for `terraform output` as `TerraformOutput` task type as well as a provider on the source set that can return all output variables as a deeply nested map.



== v0.8.0 / v0.8.1

=== Features

* {issue}17[#17] - Remote state in S3 simplified.
* {issue}18[#18] - Ability to set destruction plan from command-line.

=== Bugs

* {issue}19[#19] - `createTfS3BackendConfiguration` should run after `generateTerraformConfig`.
* {issue}20[#20] - `remote_state` map is not passed to `terraform`.
* {issue}21[#21] - `Property` and `Provider` instances are not resolved correctly to strings in `VariablesSpec`.

=== Other

* {issue}13[#13] - Remove references to legacy task naming like `terraformXYZ`.
* {mr}17[!17] - `tfShowState` will now use `.tf` extension rather than `.txt`
* Default version of terraform is 0.12.24.


== v0.7.0 / v0.7.1 / v.0.7.2

=== Bugs

* {issue}3[#3] - Task `tfShowState` fails when using remote backend.
* {mr}14[#14] - Remove unnecessary quotes from command line argument.


== v0.6.0

=== Features

* {issue}6[#6] - Bump Terraform version to 0.12.19
* {issue}7[#7] - Use `useAwsEnvironment()` as sort-cut to add all AWS-related environmental variables to Terraform execution environment. Also on non-Windows platforms add `HOME` to Terraform environment.
* {issue}9[#9] - Support for `state mv` terraform command in the form of `TerraformStateMv` task type

=== Breaking changes

* {issue}8[#8] - Instead of ``-configure-backends` use `--no-configure-backends` for a `TerraformInit` task. The task property has also been renamed to `skipConfigureBackends` (from `configureBackend`). This was done becasue a boolean command-line extension does not take a value and rather sets the task property to `true`.


== v0.5.0 / v0.5.1

=== Features

* {issue}5[#5] - Support for `state rm` terraform command in the form of `TerraformStateRm` task type

=== Bugs

* {issue}3[#3] - Task `tfShowState` fails when using remote backend

== v0.4.0

=== Features

* Support backend config for `TerraformInit`.


== v0.3.1

=== Features

* Support for `show` terraform command in the form of `TerraformShowState` task type
* `TerraformPlan` will now also generate a text or JSON-formatted report.

=== Bugs

* {issue}2[#2] - `terraformw`should pick the correct base directory when ran from anywhere within the Gradle project

=== Breaking changes

* Tasks associated with Terraform commands are now prefixed with `tf` instead of `terraform`.


== v0.2.2

=== Features

* Variables can be defined at global, source set and task level. Where variables are the same, values at task level overrides source set and in turn source set overrides global. Task can also specify that source set and global variables can be ignored completely.
* Environment can be defined at global and at task level.
* Support for `validate` and `destroy` terraform commands.

=== Bugs

* {issue}1[#1] - Variables block in source sets are creating accidental sourcesets

== v0.1.3

=== Features

* Bootstrap Terraform without installation
* Terraform source sets
* Support for `init`, `apply`, `plan` and `import` terraform commands.


