= Terraform Gradle Plugin
:author: Schalk W. Cronjé
:doctype: book
:toc: right
include::attributes.adoc[]

include::parts/introduction.adoc[]

include::parts/bootstrap.adoc[]

include::parts/platform-installation-support.adoc[]

include::parts/sourcesets.adoc[]

include::parts/terraform-extension.adoc[]

include::parts/variables.adoc[]

include::parts/environment.adoc[]

include::parts/command-line-options.adoc[]

include::parts/remote-state-s3.adoc[]

include::parts/accessing-output-variables.adoc[]

include::parts/alternatives.adoc[]
// include::parts/wrapper.adoc[]
