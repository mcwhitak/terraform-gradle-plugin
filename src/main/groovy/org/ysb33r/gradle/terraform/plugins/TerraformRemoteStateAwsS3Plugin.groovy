/*
 * Copyright 2017-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ysb33r.gradle.terraform.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.ysb33r.gradle.terraform.TerraformSourceDirectorySet
import org.ysb33r.gradle.terraform.TerraformSourceSets
import org.ysb33r.gradle.terraform.internal.remotestate.S3Conventions
import org.ysb33r.gradle.terraform.remotestate.RemoteStateS3
import org.ysb33r.gradle.terraform.remotestate.TerraformRemoteStateExtension
import org.ysb33r.grolifant.api.core.LegacyLevel

import static org.ysb33r.gradle.terraform.remotestate.TerraformRemoteStateExtension.findExtension

@CompileStatic
class TerraformRemoteStateAwsS3Plugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.apply plugin: TerraformRemoteStateBasePlugin
        RemoteStateS3 globalS3Configuration = ((ExtensionAware) findExtension(project)).extensions.create(
            RemoteStateS3.NAME,
            RemoteStateS3,
            project
        )

        project.extensions.getByType(TerraformSourceSets).all({ TerraformSourceDirectorySet tsds ->
            def trse = (ExtensionAware) ((ExtensionAware) tsds).extensions.getByType(TerraformRemoteStateExtension)
            def remote = trse.extensions.create(RemoteStateS3.NAME, RemoteStateS3, project)

            remote.follow(globalS3Configuration)

            if (LegacyLevel.PRE_4_10) {
                S3Conventions.taskCreator(project, tsds, remote)
            } else {
                S3Conventions.taskLazyCreator(project, tsds, remote)
            }
        } as Action<TerraformSourceDirectorySet>)

        project.apply plugin: TerraformPlugin
    }
}
